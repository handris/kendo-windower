import { module } from 'angular';
import Root from "./components/root.component";


export const moduleName = module('application', [])
        .component(Root.selector, Root).name;
