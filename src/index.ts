import "./main.scss";
import { moduleName as appModule } from './app/app.module';
import * as angular from "angular";

angular.module('application.bootstrap', [appModule]);
